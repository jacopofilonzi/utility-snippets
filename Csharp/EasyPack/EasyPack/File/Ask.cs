﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Windows.Forms;


namespace EasyPack.File
{
    public static partial class Ask
    {

        #region  Checkers

        /// <summary>
        /// Check if a folder exist
        /// </summary>
        /// <param name="path">Path of the folder</param>
        /// <returns>Boolean</returns>
        public static bool DirectoryExist(string path)
        {
            return System.IO.Directory.Exists(path);
        }

        /// <summary>
        /// Check if a file exist
        /// </summary>
        /// <param name="path">Path of the file to check</param>
        /// <returns>Boolean</returns>
        public static bool FileExist(string path)
        {
            return System.IO.File.Exists(path);
        }

        #endregion

        #region modals

        /// <summary>
        /// Allow the user to input a file
        /// </summary>
        /// <param name="filters">Define the tipes of file alowed</param>
        /// <param name="selectedFilterIndex">Default filter initialy applied</param>
        /// <param name="multipleSelect">Alow selection of multiple files</param>
        /// <param name="initialDirectory">Initial directory where the user is prompted to</param>
        /// <returns>Array of paths(string)</returns>
        public static string[] FilesPath(ExtensionFilter[] filters = null, int selectedFilterIndex = 0, bool multipleSelect = false, string initialDirectory = null)
        {
            //OpenFileDialog istance
            OpenFileDialog ofd = new OpenFileDialog();

            //Check if the given path file exist
            ofd.CheckFileExists = true;

            //Check if filters has been declared
            if (filters == null || filters.Length == 0)
                ofd.Filter = "All Files (*.*)|*.*";
            else//
            {
                //Default filter index
                ofd.FilterIndex = selectedFilterIndex;


                //For eache filter 
                for (int i = 0; i < filters.Length; i++)
                {
                    //If not the first filter
                    if (i != 0)//Filters string separator
                        ofd.Filter += "|";

                    //Add filter to the filters string
                    ofd.Filter += $"{filters[i].name} ({filters[i].extensionType})|{filters[i].extensionType}";
                }
            }


            //Allow or not multiple file selection
            ofd.Multiselect = multipleSelect;

            //If an initial directory has been given
            if (initialDirectory != null)
                ofd.InitialDirectory = initialDirectory;

            //Show the modal
            DialogResult dr = ofd.ShowDialog();


            //If the user selected something
            if (dr == DialogResult.OK)
                return ofd.FileNames;
            else //Else the user cancelled
                return null;


        }

        /// <summary>
        /// TODO: review - Allow the user to input where to save a file
        /// </summary>
        /// <param name="filters"></param>
        /// <param name="filterIndex"></param>
        /// <param name="initialDirectory"></param>
        /// <returns></returns>
        public static string SaveFilePath(ExtensionFilter[] filters = null, int selectedFilterIndex = 0, string initialDirectory = null)
        {
            //SaveFileDialog istance
            SaveFileDialog sfd = new SaveFileDialog();

            //Crea un nuovo file se non esiste
            sfd.CheckFileExists = false;

            //Ask to overwrite an existing file if there is one
            sfd.OverwritePrompt = true;

            //Ask to create a file if not exist
            sfd.CreatePrompt = false;

            //Extension filters
            if (filters == null)
                sfd.Filter = "Tutti i file (*.*)|*.*";
            else
            {
                for (int i = 0; i < filters.Length; i++)
                {
                    if (i != 0)
                        sfd.Filter += "|";

                    sfd.Filter += $"{filters[i].name} ({filters[i].extensionType})|{filters[i].extensionType}";
                }
            }

            //Filter index
            sfd.FilterIndex = selectedFilterIndex;

            //Set initial directory if specified
            if (initialDirectory != null)
                sfd.InitialDirectory = initialDirectory;

            //Show modal
            DialogResult dr = sfd.ShowDialog();

            //If user selected something
            if (dr == DialogResult.OK)
                return sfd.FileName;
            else
                return null;
        }

        /// <summary>
        /// Allow the user to input a folder
        /// </summary>
        /// <returns>Path of the selected folder </returns>
        public static string FoldersPath()
        {
            //FolderBrowserDialog istance
            FolderBrowserDialog fbd = new FolderBrowserDialog();

            DialogResult dr = fbd.ShowDialog();
            if (dr == DialogResult.OK)
                return fbd.SelectedPath;
            else
                return null;
        }
        
        #endregion
    }
}
