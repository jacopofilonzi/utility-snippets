﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasyPack.File
{
    public static partial class Ask
    {
        /// <summary>
        /// The extension filter is used for the dialogs of the file selection request.
        /// 
        /// It has a name that stands for the visualized one by the user and an extension type that could be "*.txt".
        /// 
        /// "*" stands for "anything" so you could do "*.*" that alow the user to select every file that he want.
        /// </summary>
        public struct ExtensionFilter
        {
            public string name;
            public string extensionType;
        }

    }
}
