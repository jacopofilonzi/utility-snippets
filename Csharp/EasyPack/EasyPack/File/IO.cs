﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


/* EasyPack - File - IO (Input - Output)
 *
 * Package Author: Filonzi Jacopo;
 * Description:    This package offers functionality for reading and writing files
 * Gitlab:         https://gitlab.com/jacopofilonzi
 */


namespace EasyPack.File
{
    public static class IO
    {

        /// <summary>
        /// Read a file
        /// </summary>
        /// <param name="path">Path of the file to read</param>
        /// <returns>Array of rows of the file</returns>
        public static List<string> Read(string path)
        {
            //The output list
            List<string> Rows = new List<string>();

            //StreamReader object
            System.IO.StreamReader sr = new System.IO.StreamReader(path);

            //Current line readed
            string currentLine = sr.ReadLine();

            //Untill there is no more lines
            while(currentLine != null)
            {
                //Add the current line to the Rows list
                Rows.Add(currentLine);

                //Try to read the next line
                currentLine = sr.ReadLine();
            }

            //Close the reading
            sr.Close();

            //Return the List of rows
            return Rows;
        }



        /// <summary>
        /// Write a file
        /// </summary>
        /// <param name="path">Path of the file</param>
        /// <param name="rows">List of rows to write</param>
        /// <returns>void</returns>
        public static void Write(string path, List<string> rows)
        {
            //StreamWriter object
            System.IO.StreamWriter sw = new System.IO.StreamWriter(path);

            //For each line given
            foreach (string row in rows)
            {
                //Write it into the file
                sw.WriteLine(row);
            }

            //Close the connection
            sw.Close();
            
        }

    }
}
