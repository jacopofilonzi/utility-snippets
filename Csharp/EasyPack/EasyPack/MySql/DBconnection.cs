﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;


namespace EasyPack.MySql
{
    public class DBconnection
    {
        #region Variables
        private string connectionString;
        private MySqlConnection connection;
        #endregion

        #region Costructor
        public DBconnection(string connectionString)
        {
            //Save connection string
            this.connectionString = connectionString;
            //Create the database istance
            this.connection = new MySqlConnection(this.connectionString);
        }
        #endregion

        #region functions

        /// <summary>
        /// Open the connection to the database
        /// </summary>
        public void openConnection()
        {
            connection.Open();
        }

        /// <summary>
        /// Close the connection to the database
        /// </summary>
        public void closeConnection()
        {
            connection.Close();
        }

        /// <summary>
        /// Make a query into the database
        /// </summary>
        /// <param name="query">Query to input</param>
        /// <param name="paramethers">NOT IMPLEMENTED - paramethers to pass into the query "?"</param>
        /// <returns></returns>
        public MySqlDataReader query(string query, string[] paramethers)
        {
            openConnection();

            //Make the command object
            MySqlCommand cmd = new MySqlCommand(query, this.connection);

            //Send the query and store the result
            MySqlDataReader dr = cmd.ExecuteReader();

            closeConnection();

            //Return the result
            return dr;
        }

        #endregion
    }
}
