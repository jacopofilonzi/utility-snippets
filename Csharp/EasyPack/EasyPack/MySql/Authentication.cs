﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasyPack.MySql
{
    public class DBauthentication
    {
        #region Variables
        private string host;
        private int port;

        private string username;
        private string password;

        private string database;
        #endregion

        #region Properties

        /// <summary>
        /// Return the connection string to be used in the connection
        /// </summary>
        public string ConnectionString
        {
            get
            {
                return $"server={this.host};user={this.username};password={this.password};database={this.database};";
            }
        }
        #endregion

        #region constructor
        /// <summary>
        /// This class helps with the connection string
        /// </summary>
        /// <param name="username">Username used for the authentication</param>
        /// <param name="password">Password used for the authentication</param>
        /// <param name="database">Name of the database to use in the connection</param>
        /// <param name="host">Host where the database is located</param>
        /// <param name="port">Port used from the database</param>
        public DBauthentication(string username, string password, string database, string host = "127.0.0.1", int port = 3306)
        {
            this.host = host;
            this.port = port;

            this.username = username;
            this.password = password;

            this.database = database;
        }
        #endregion


    }
}
