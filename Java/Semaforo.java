/**
 * Classe utile all'utilizzo di risorse con disponibilità limitata
 * 
 * @author Filonzi Jacopo 
 * @version 1.0
 */
public class Semaforo
{
    private int _maximum;

    public Semaforo(int maximum)
    {
        this._maximum = maximum;
    }

    //Occupa il semaforo
    synchronized public void P()
    {
        //Utilizza la risorsa
        //==> Usata per entrare nella sezione critica
        
        if (_maximum <= 0)
            try {wait();} catch (InterruptedException e) {}

        this._maximum--; //Prendi il posto
    }

    //Libera il semaforo
    synchronized public void V()
    {
        //Liberare la risorsa
        //==> Usata per l'uscita dalla sezione critica

        
        notify(); //Notifica un cambio di condizione e risveglia dalla lista dei thread sospesi il primo thread
        this._maximum++; //Libera il posto
    }

}