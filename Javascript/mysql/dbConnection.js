/**
 * Class for the connection to a database that implements the "mysql" module
 * WARNING:
 * This class works with nodejs and use a specific module "mysql"
 * $ npm install mysql
 */
export default class dbConnection {
    //Public variables
    datdabaseConnection = null;


    /** Database authentication structure
     * This is the structure for how the authentication object has to be compiled
     * @param {string} host - the host where the database is
     * @param {string} user - the username used for the authentication
     * @param {string} password - the password used for the authenticatio
     * @param {string} [database] - the database to use in the connection (optional)
     */
     authentication = {}


    constructor(host, user, password, database = undefined) {
        //Set paramethers for authentication
        this.authentication.host = host;
        this.authentication.user = user;
        this.authentication.password = password;

        //if database selected
        database != undefined ? this.authentication.database = database : null;

        //Create the bridge
        this.databaseConnection = require("mysql").createConnection(this.authentication);

    }

    /** Asynchronous function for sending a custom query
     * this function will help with the using of a query in a database
     * @param {string} query - Query to execute
     * @param {Array.<string|number>} [params] - Parameters to send in the query (they will replace the question marks "?" in the query) (optional)
     * @param {auth} [auth] - Parameters for the connection to the database (optional)
     * @returns {Object} - returns the results and the fields of the query
     * @property {Array} results - the results given from the query
     * @property {Array} fields - the fields (or columns) used in the result of the query
     */
    async query(query, params) {

        // Connect to the database
        this.databaseConnection.connect();

        // Return a promise for the query
        return new Promise((resolve, reject) => {
            this.databaseConnection.query(query, params, (error, results, fields) => {

                // Close the connection to the database
                this.databaseConnection.end();

                if (error) // There was an error in the query
                    reject(error);
                else // The query ran successfully
                    resolve({ results: results, fields: fields });
            });
        });

    }
}

